package kg.attractor.microgram.util;
import kg.attractor.microgram.model.Comment;
import kg.attractor.microgram.model.CommentRepository;
import kg.attractor.microgram.model.User;
import kg.attractor.microgram.model.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDateTime;
import java.util.Random;
import java.util.stream.Stream;


@Configuration
public class PreloadDatabaseWithData {

    @Bean
    CommandLineRunner initUserDatabase(UserRepository repository) {

        repository.deleteAll();

        return (args) -> Stream.of(users())
                .peek(System.out::println)
                .forEach(repository::save);
    }

    @Bean
    CommandLineRunner initCommentDatabase(CommentRepository repository) {

        repository.deleteAll();

        return (args) -> Stream.of(comments())
                .peek(System.out::println)
                .forEach(repository::save);
    }
    private User[] users() {
        return new User[]{
                new User ("SanaK@gmail.com", "Sana", "12345"),
                new User ("Nazira@gmail.com", "Nazira", "20211"),
                new User ("Azer@gmail.com", "Azer", "realmadrid"),
                new User ("Jama@gmail.com", "Jama", "2020"),
                new User ("Bema@gmail.com", "Bema", "password"),
                new User("Keka@gmail.com", "Keka", "embr.k")};
    }
    private Comment[] comments() {
        return new Comment[]{
                new Comment ("1", "SanaK@gmail.com", "It was good day!", getTime()),
                new Comment ("2", "Bema@gmail.com", "Yeah, perfect day!", getTime()),
                new Comment ("3", "Keka@gmail.com", "Hello world", getTime()),
                new Comment ("4", "SanaK@gmail.com", "Somebody once told me", getTime()),
                new Comment ("5", "Nazira@gmail.com", "We are the champions", getTime()),
                new Comment("6", "Bema@gmail.com", "Blade Runner 2049", getTime())};
    }
    private LocalDateTime getTime(){
        LocalDateTime time = LocalDateTime.now();
        Random rnd = new Random();
        int x = rnd.nextInt(100);
        time.minusHours(x);
        return time;
    }
}
