package kg.attractor.microgram.model;

import org.springframework.data.repository.CrudRepository;

public interface LikeRepository extends CrudRepository<Like, String> {
    //Найти посты с лайками юзера по юзеру
}
