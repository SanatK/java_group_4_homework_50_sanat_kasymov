package kg.attractor.microgram.model;

import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository <User, String>{
}
