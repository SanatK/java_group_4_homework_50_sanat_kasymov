package kg.attractor.microgram.model;

import org.springframework.data.repository.CrudRepository;

public interface SubscribesRepository extends CrudRepository<Subscribes, String> {
}
