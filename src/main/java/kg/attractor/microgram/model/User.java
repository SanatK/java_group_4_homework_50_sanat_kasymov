package kg.attractor.microgram.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;

@Document
public class User {
    @Id
    private String email;
    private String name;
    private String password;
    private int postsCounter;
    private int subscribesCounter;
    private int subscribersCounter;

    public User(String email, String name, String password) {
        Objects.requireNonNull(email);
        Objects.requireNonNull(name);
        Objects.requireNonNull(password);
        this.email = email;
        this.name = name;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getPostsCounter() {
        return postsCounter;
    }

    public void setPostsCounter(int postsCounter) {
        this.postsCounter = postsCounter;
    }

    public int getSubscribesCounter() {
        return subscribesCounter;
    }

    public void setSubscribesCounter(int subscribesCounter) {
        this.subscribesCounter = subscribesCounter;
    }

    public int getSubscribersCounter() {
        return subscribersCounter;
    }

    public void setSubscribersCounter(int subscribersCounter) {
        this.subscribersCounter = subscribersCounter;
    }
}
