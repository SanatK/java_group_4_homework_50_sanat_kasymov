package kg.attractor.microgram.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Document
public class Subscribes {
    @Id
    private String id;
    private User newSubscriber;
    private User user;
    private LocalDateTime subscriptionTime;
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public User getNewSubscriber() {
        return newSubscriber;
    }

    public void setNewSubscriber(User newSubscriber) {
        this.newSubscriber = newSubscriber;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LocalDateTime getSubscriptionTime() {
        return subscriptionTime;
    }

    public void setSubscriptionTime(LocalDateTime subscriptionTime) {
        this.subscriptionTime = subscriptionTime;
    }
    
}
